package test;

import org.junit.Test;
import org.trevans.objectsToFix.BinarySearch;

import static org.junit.Assert.assertEquals;

public class BinarySearchTest {

    @Test (timeout = 1000)
    public void testReturnsNegativeOneForEmptyArray(){
        assertEquals(-1, BinarySearch.binarySearch(new int[0], 1));
    }

    @Test (timeout = 1000)
    public void testReturnsAddressOfSingleElement(){
        assertEquals(0, BinarySearch.binarySearch(new int[]{1}, 1));
    }

    @Test (timeout = 1000)
    public void testReturnsNegativeOneIfNotInSingleElementList(){
        assertEquals(-1, BinarySearch.binarySearch(new int[]{1}, 0));
    }

    @Test (timeout = 1000)
    public void testReturnsAddressOfElementInLongList(){
        assertEquals(2, BinarySearch.binarySearch(new int[]{0,1,2,3,4,5,6,7,8,9}, 2));
    }


    @Test (timeout = 1000)
    public void testReturnsNegativeOneIfElementNotInInLongList(){
        assertEquals(-1, BinarySearch.binarySearch(new int[]{0,1,2,3,4,5,6,7,8,9}, 10));
    }

    @Test (timeout = 1000)
    public void testReturnsAddressOfNegativeElementInList(){
        assertEquals(1, BinarySearch.binarySearch(new int[]{-2,-1,0,1,2,3}, -1));
    }

    @Test (timeout = 1000)
    public void testReturnsAddressOfElementWhenNumbersAreNotSequential(){
        assertEquals(2, BinarySearch.binarySearch(new int[]{1,3,5,7,9,11,13,15}, 5));
    }

    @Test (timeout = 1000)
    public void testReturnsAddressOfEndpointsOfArray(){
        int[] testArray = new int[] {1,2,3,4,5,6,7,8,9};
        assertEquals(0, BinarySearch.binarySearch(testArray, 1));
        assertEquals(8, BinarySearch.binarySearch(testArray, 9));
    }

    @Test (timeout = 1000)
    public void testReturnsAddressInOddLengthArray(){
        assertEquals(3, BinarySearch.binarySearch(new int[]{0,1,2,3,4,5,6,7,8}, 3));
    }
}
