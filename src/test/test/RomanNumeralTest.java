package test;

import org.junit.Test;
import org.trevans.objectsToFix.RomanNumeral;

import static org.junit.Assert.*;

public class RomanNumeralTest {

    @Test
    public void test1BecomesI(){
        assertEquals("I", RomanNumeral.fromInt(1));
    }

    @Test
    public void test5BecomesV(){
        assertEquals("V", RomanNumeral.fromInt(5));
    }

    @Test
    public void test10BecomesX(){
        assertEquals("X", RomanNumeral.fromInt(10));
    }

    @Test
    public void test50BecomesL(){
        assertEquals("L", RomanNumeral.fromInt(50));
    }

    @Test
    public void test100BecomesC(){
        assertEquals("C", RomanNumeral.fromInt(100));
    }

    @Test
    public void test500BecomesD(){
        assertEquals("D", RomanNumeral.fromInt(500));
    }

    @Test
    public void test1000BecomesM(){
        assertEquals("M", RomanNumeral.fromInt(1000));
    }

    @Test
    public void test4BecomesIV(){
        assertEquals("IV", RomanNumeral.fromInt(4));
    }

    @Test
    public void test9BecomesIX(){
        assertEquals("IX", RomanNumeral.fromInt(9));
    }

    @Test
    public void test90BecomesXC(){
        assertEquals("XC", RomanNumeral.fromInt(90));
    }

    @Test
    public void test400BecomesCD(){
        assertEquals("CD", RomanNumeral.fromInt(400));
    }

    @Test
    public void test900BecomesCM(){
        assertEquals("CM", RomanNumeral.fromInt(900));
    }

    @Test
    public void test99BecomesXCIX(){
        assertEquals("XCIX", RomanNumeral.fromInt(99));
    }

    @Test
    public void test8BecomesVIII(){
        assertEquals("VIII", RomanNumeral.fromInt(8));
    }

    @Test
    public void test2496BecomesMMCDXCVI(){
        assertEquals("MMCDXCVI", RomanNumeral.fromInt(2496));
    }
}
