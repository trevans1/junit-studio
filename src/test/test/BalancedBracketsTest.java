package test;

import org.junit.Test;
import org.trevans.objectsToFix.BalancedBrackets;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BalancedBracketsTest {

    @Test
    public void testReturnsTrueIfBracketsAreBalanced(){
        assertTrue(BalancedBrackets.hasBalancedBrackets("[]"));
    }

    @Test
    public void testReturnsFalseIfOpenBracketIsNotClosed(){
        assertFalse(BalancedBrackets.hasBalancedBrackets("["));
    }

    @Test
    public void testReturnsFalseIfBracketsAreInReverseOrder(){
        assertFalse(BalancedBrackets.hasBalancedBrackets("]["));
    }

    @Test
    public void testReturnsFalseIfUnexpectedCloseBracket(){
        assertFalse(BalancedBrackets.hasBalancedBrackets("]"));
    }

    @Test
    public void testReturnsTrueIfBracketsAreBalancedWithInsideCharacters(){
        assertTrue(BalancedBrackets.hasBalancedBrackets("[balanced]"));
    }

    @Test
    public void testReturnsTrueIfBracketsAreBalancedWithInsideCharactersIncludingPunctuation(){
        assertTrue(BalancedBrackets.hasBalancedBrackets("[This is balanced!]"));
    }

    @Test
    public void testReturnsFalseIfBracketsNotBalancedWithInsideCharacters(){
        assertFalse(BalancedBrackets.hasBalancedBrackets("[This is unbalanced!"));
    }

    @Test
    public void testReturnsTrueWhenMultipleNestedBracketsAreBalanced(){
        assertTrue(BalancedBrackets.hasBalancedBrackets("[[]][[][]][]"));
    }

    @Test
    public void testReturnsFalseWhenMultipleNestedBracketsAreNotBalanced(){
        assertFalse(BalancedBrackets.hasBalancedBrackets("][[["));
    }
}
