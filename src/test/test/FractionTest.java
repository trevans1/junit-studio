package test;

import org.junit.Before;
import org.junit.Test;
import org.trevans.objectsToFix.Fraction;

import static org.junit.Assert.*;

public class FractionTest {
    Fraction fraction;

    @Before
    public void createFraction(){
        fraction = new Fraction(1,4);
    }

    @Test
    public void testFractionIsCreatedCorrectly(){
        assertEquals((Integer)1, fraction.getNumerator());
        assertEquals((Integer)4, fraction.getDenominator());
    }

    @Test
    public void testEqualsReturnsTrueWhenFractionsAreEqual(){
        Fraction f = new Fraction(fraction.getNumerator(), fraction.getDenominator());
        assertTrue(fraction.equals(f));
    }

    @Test
    public void testEqualsReturnsFalseWhenFractionsAreNotEqual(){
        Fraction f = new Fraction(7/12309);
        assertFalse(fraction.equals(f));
    }


    @Test
    public void testFractionsWithSameDenominatorAreAddedCorrectly(){
        Fraction expected = new Fraction(1,2);
        Fraction toAdd = new Fraction(1,4);
        assertTrue(expected.equals(fraction.add(toAdd)));
    }

    @Test
    public void testFractionsWithDifferentDenominatorsAreAddedCorrectly(){
        Fraction expected = new Fraction(3,4);
        Fraction toAdd = new Fraction(1,2);
        assertTrue(expected.equals(fraction.add(toAdd)));
    }

    @Test
    public void testCompareToReturnsZeroWhenFractionsAreTheSame(){
        Fraction f = new Fraction(fraction.getNumerator(), fraction.getDenominator());
        assertEquals(0, fraction.compareTo(f));
    }

    @Test
    public void testCompareToReturnsGreaterThanZeroIfSecondFractionIsSmaller(){
        Fraction f = new Fraction(fraction.getNumerator(), fraction.getDenominator()*2);
        int compared = fraction.compareTo(f);
        if(compared <= 0){
            fail("Value was " + compared);
        }
    }

    @Test
    public void testCompareToReturnsLessThanZeroIfSecondFractionIsGreater(){
        Fraction f = new Fraction(fraction.getNumerator(), fraction.getDenominator()/2);
        int compared = fraction.compareTo(f);
        if(compared >= 0){
            fail("Value was " + compared);
        }
    }

    @Test
    public void testGcdReturnsCorrectValueWhenMGreaterThanN(){
        Fraction f = new Fraction(2,1);
        assertTrue(f.equals(new Fraction(8/4)));
    }

    @Test
    public void testAddFractionsWithNumeratorsGreaterThanOne(){
        Fraction f1 = new Fraction(2,3);
        Fraction f2 = new Fraction(1,3);
        Fraction expected = new Fraction(1,1);
        assertTrue(expected.equals(f1.add(f2)));
    }

    @Test
    public void testAddFractionsThatHaveDifferentNumeratorsAndDenominators(){
        Fraction f1 = new Fraction(2,3);
        Fraction f2 = new Fraction(1,4);
        Fraction expected = new Fraction(11,12);
        assertTrue(expected.equals(f1.add(f2)));
    }
}
